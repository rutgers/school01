#!/bin/bash
# Personen

echo "Voer de eerste naam in, gevolgd door [ENTER] >"
read naam1

echo "Voer de tweede naam in, gevolgd door [ENTER] >"
read naam2

echo "Voer de derde naam in, gevolgd door [ENTER] >"
read naam3

echo "Voer de leeftijd van $naam1 in, gevolgd door [ENTER] >"
read leeftijd1

echo "Voer de leeftijd van $naam2 in, gevolgd door [ENTER] >"
read leeftijd2

echo "Voer de leeftijd van $naam3 in, gevolgd door [ENTER] >"
read leeftijd3


echo "<?xml version='1.0' encoding='UTF-8'?>
<persons>
<person>
<name>$naam1</name>
<age>$leeftijd1</age>
</person>
<person>
<name>$naam2</name>
<age>$leeftijd2</age>
</person>
<person>
<name>$naam3</name>
<age>$leeftijd3</age>
</person>
</persons>" > /tmp/personen.xml



