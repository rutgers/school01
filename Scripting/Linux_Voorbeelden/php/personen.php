<?php
if(isset($_GET['naam1']) && isset($_GET['naam2']) && isset($_GET['naam3']) && isset($_GET['leeftijd1']) && isset($_GET['leeftijd2']) && isset($_GET['leeftijd3']))
{
	$naam1 = $_GET['naam1'];
	$naam2 = $_GET['naam2'];
	$naam3 = $_GET['naam3'];
	$leeftijd1 = $_GET['leeftijd1'];
	$leeftijd2 = $_GET['leeftijd2'];
	$leeftijd3 = $_GET['leeftijd3'];
	
	//Output XML
	header('Content-Type: text/xml');
	echo "
	<persons>
		<person>
			<name>" . $naam1 . "</name>
			<age>" . $leeftijd1 . "</age>
		</person>
		<person>
			<name>" . $naam2 . "</name>
			<age>" . $leeftijd2 . "</age>
		</person>
		<person>
			<name>" . $naam3 . "</name>
			<age>" . $leeftijd3 . "</age>
		</person>
	</persons>
	";
	exit;
}
if(isset($_GET['naam1']) && isset($_GET['naam2']) && isset($_GET['naam3']))
{
	$naam1 = $_GET['naam1'];
	$naam2 = $_GET['naam2'];
	$naam3 = $_GET['naam3'];
	
	echo "
	<html>
		<head>
			<title>Leeftijden</title>
		</head> 
		<body>
			<form action=\"personen.php\" method=\"get\">
				<input type=\"hidden\" name=\"naam1\" value=\"$naam1\">
				<input type=\"hidden\" name=\"naam2\" value=\"$naam2\">
				<input type=\"hidden\" name=\"naam3\" value=\"$naam3\">
				Wat is de leeftijd van $naam1: <input type=\"text\" name=\"leeftijd1\"><br>
				Wat is de leeftijd van $naam2: <input type=\"text\" name=\"leeftijd2\"><br>
				Wat is de leeftijd van $naam3: <input type=\"text\" name=\"leeftijd3\"><br>
				<input type=\"submit\" value=\"Invoeren\">
			</form>
		</body>
	</html>";
	exit;
}
else
{
	echo "
	<html>
		<head>
			<title>Namen</title>
		</head> 
		<body>
			<form action=\"personen.php\" method=\"get\">
				Wat is de naam van persoon 1: <input type=\"text\" name=\"naam1\"><br>
				Wat is de naam van persoon 2: <input type=\"text\" name=\"naam2\"><br>
				Wat is de naam van persoon 3: <input type=\"text\" name=\"naam3\"><br>
				<input type=\"submit\" value=\"Invoeren\">
			</form>
		</body>
	</html>";
	exit;
}
?>
