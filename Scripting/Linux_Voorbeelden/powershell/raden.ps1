﻿$geraden = Read-Host -Prompt 'Voer een getal van 0 tot 9 in, gevolgd door [ENTER] >'
$getal = Get-Random -minimum 0 -maximum 9

if($getal -eq $geraden)
{
    echo "Je hebt het getal goed geraden, het getal was $getal"
}
else
{
    echo "Je hebt $geraden geraden, het getal was $getal"
}