﻿$persoon1 = Read-Host -Prompt 'Voer de eerste naam in, gevolgd door [ENTER] >'
$persoon2 = Read-Host -Prompt 'Voer de tweede naam in, gevolgd door [ENTER] >'
$persoon3 = Read-Host -Prompt 'Voer de derde naam in, gevolgd door [ENTER] >'

$persoon1leeftijd = Read-Host -Prompt "Voer de leeftijd van $persoon1 in, gevolgd door [ENTER] >"
$persoon2leeftijd = Read-Host -Prompt "Voer de leeftijd van $persoon2 in, gevolgd door [ENTER] >"
$persoon3leeftijd = Read-Host -Prompt "Voer de leeftijd van $persoon3 in, gevolgd door [ENTER] >"

Write-Host "Namen: $persoon1 $persoon2 $persoon3 $persoon1leeftijd $persoon2leeftijd $persoon3leeftijd"

$personenLijst = '<?xml version="1.0" encoding="UTF-8"?>
<persons>
		<person>
			<name>' + $persoon1 + '</name>
			<age>' + $persoon1leeftijd +'</age>
		</person>
		<person>
			<name>' + $persoon2 + '</name>
			<age>' + $persoon2leeftijd +'</age>
		</person>
		<person>
			<name>' + $persoon3 + '</name>
			<age>' + $persoon3leeftijd +'</age>
		</person>
	</persons>'

out-file -filepath .\personen.xml -inputobject $personenLijst
