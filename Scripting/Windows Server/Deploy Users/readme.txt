addusers.ps1 is het script dat de gebruikers toevoegd

In include staan de volgende bestanden:
common.ps1 	Hierin staan alle instellingen die je eventueel zou willen aanpassen (bewerken met notepad of powershell ISE)
ou.struct 	Hierin staan alle OU's die aangemaakt moeten worden (bewerken met notepad)
users.csv	Hierin staan de gebruikers met hun persoonlijke gegevens en OU (bewerken met notepad of excel)

Pas in common.ps1 de servernaam en de domeinnaam aan, en de instellingen die je verder nodig hebt
Zet in ou.struct de OU's die aangemaakt moeten worden
Zet in users.csv de gebruikers die aangemaakt moeten worden.

Run addusers.ps1 en kijk of er geen foutmeldingen verschijnen, als er 1x een foutmelding is is het waarschijnlijk een fout in de gebruikersinvoer of de invoer van OU's. Bij meerdere fouten is er waarschijnlijk een tikfout gemaakt in common.ps1

Het script maakt eerst de OU's aan. Voor elke OU wordt er ook een security group aangemaakt.
Daarna maakt het script de gebruikers aan, en plaatst ze vervolgens in de juiste OU en security group.