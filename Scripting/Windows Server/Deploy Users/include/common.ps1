##########################################################################
#     Variables used in the other scripts
#     (c) Rutger Steenwijk 2016/2017
#     Version: V1.3.1 06/04/2017 Voorbeeld voor stagiairs
##########################################################################

##########################################################################
# General variables
##########################################################################
$scriptVersion = "V1.3.1 06/04/2017 Voorbeeld voor stagiairs"
$scriptBranding = "(c) Rutger Steenwijk 2016/2017"

##########################################################################
# Lists
##########################################################################
$CSVUserList = Import-csv $PSScriptRoot\users.csv
$CSVOUStruct = Get-Content $PSScriptRoot\ou.struct

##########################################################################
# Domain variables
##########################################################################
$serverName = "ServerNaamHierInvullen"
$domainName = "DomeinNaamHierInvullen" #zonder extensie
$domainExtention = "int" #domeinextensie, zonder punt: bijv. nl com local lan org 
$defaultUserPassword = "Welkom01"


##########################################################################
# DFS
##########################################################################
$DFSUseDFS = $false

$DFSUserProfilesFolderName = "UserProfiles"
$DFSUserFoldersFolderName = "UserFolders"

##########################################################################
# OU settings
##########################################################################
$protectOU = 0
$rootOU = "JouHoofdAfdeling/OU/de root OU"
$ouRootPath = �OU=$rootOU,DC=$domainName,DC=$domainExtention�

##########################################################################
# Group settings
##########################################################################
$SecurityGroupPrefix = "SG_"

##########################################################################
# User settings
##########################################################################
$fUserNameStyle = $false
$defaultEnableAccount = 1
$requirePasswordChange = 0
$logonScriptPath = ""
$defaultHomeDriveLetter = "Z:"