##########################################################################
#     Variables used in the other scripts
#     (c) Rutger Steenwijk 2016/2017
#     Version: V1.3.1 06/04/2017 Voorbeeld voor stagiairs
##########################################################################

#All settings are contained in a single location
. $PSScriptRoot\include\common.ps1

##########################################################################
# Create OU's
##########################################################################
$firstRound = $true
$mainDeptName = ""
foreach ($_ in $CSVOUStruct) {
    #Variables
    $FQDN = "DC=$domainName,DC=$domainExtention"
    $FullOUPath = ''
    $OUPath = (Split-Path $_ -Parent).Split('\')

    [array]::Reverse($OUPath)

    $OUPath | Foreach-Object {

        if ($_.Length -eq 0) {
            return
        }

        $FullOUPath = $FullOUPath + 'OU=' + $_ + ','
    }

    #Variables
    $FullOUPath += $FQDN
    $OUName = Split-Path $_ -Leaf

    "Creating OU: $OUName in $FullOUPath"
    New-ADOrganizationalUnit -Name $OUName -Path $FullOUPath -ProtectedFromAccidentalDeletion $protectOU

    #Hack
    if($firstRound)
    {
        $firstRound = $false
        $mainDeptName = $OUName
        write-host "WARNING: While creating groups I assume $mainDeptName is the root container and therefor I will not create a group for this container. Correct manualy if needed." -foreground "Red"
    }

    if($OUName -ne $mainDeptName)
    {
        $FullOUPath = 'OU=' + $OUName + ',' + $FullOUPath
        $FullGroupName = $SecurityGroupPrefix + $OUName.ToString()
        "Creating Group: $FullGroupName in $FullOUPath"
        New-ADGroup -Name $FullGroupName -SamAccountName $FullGroupName -GroupCategory Security -GroupScope Global -DisplayName $FullGroupName -Path $FullOUPath -Description "Members of this group are part of the $OUName department."  
    }
}

##########################################################################
# Create Users in the correct OU
##########################################################################  
foreach ($user in $CSVUserList)
{  
  #Get and set all these variables
  $firstname = $user.firstname
  $lastname = $user.lastname
  
  #Build username
  if($fUserNameStyle)
  {
    #firstletterlastname :: apietersen
    $userFirstChar = $firstname.Substring(0,1) 
    $username = $userFirstChar.ToLower() + $lastname.ToLower()
  }
  else
  {
    #firstname.lastname :: anna.pietersen
    $username = $firstname.ToLower() + "." + $lastname.ToLower()
  }

  $username = $username -replace '\s','' #Replace whitespace
  if($username.Length -ge 20) #max lenght == 20
  {
     #"Username too long, trimming..."
     $username = $username.Substring(0,20)
  }

  $OU = $user.OU
  $OUPath = �OU=$OU,OU=$rootOU,DC=$domainName,DC=$domainExtention� # we can't use this
  $OUUN = $user.OUUN + $ouRootPath
  $userProfilePath = "C:\NotImplementedInThisScript\" + "$username"
  $userHomePath = "C:\NotImplementedInThisScriptv2\" + "$username"

  #Misc unneeded extras
  $userCity = $user.city
  $userCompany = $user.company
  $userCountry = $user.country             
  $userDepartment = $OU #lazy
  $userDisplayName  = $firstname + " " + $lastname  #lazy       
  $userOrganization  = $user.company #lazy
  $userState  = $user.state 

  #Check if we use DFS, if not, edit the homepath to reflect the current server, if you store profiles on a dedicated server, change $servername to the required servername
  if(!($DFSUseDFS))
  {
    #Assume 'AMF' the first server contains the user shares
    $userProfilePath = "\\" + $serverName + "\" + $DFSUserProfilesFolderName + "\" + $username
    $userHomePath =  "\\" + $serverName + "\" + $DFSUserFoldersFolderName + "\" +  $username
  }

  "Creating user: $rootOU\$OU\$username" 
  new-aduser -samaccountname $username `
  -name "$firstname $lastname" `
  -givenname $firstname `
  -surname $lastname `
  -enabled $defaultEnableAccount `
  -ProfilePath $userProfilePath `
  -HomeDirectory $userHomePath `
  -HomeDrive $defaultHomeDriveLetter `
  -scriptpath $logonScriptPath `
  -accountpassword (convertto-securestring $defaultUserPassword -asplaintext -force) `
  -ChangePasswordAtLogon $requirePasswordChange `
  -Path $OUUN `
  -City $userCity `
  -Company $userCompany `
  -Country $userCountry `
  -Department $userDepartment `
  -DisplayName $userDisplayName `
  -Organization $userOrganization `
  -State $userState

  "Creating homepath: $userHomePath" 
  md -Path $userHomePath

  #Adding user to group
  $FullGroupName = $SecurityGroupPrefix + $OU.ToString()
  "Adding $username to group: $FullGroupName"
  Add-ADGroupMember $FullGroupName $username
}  