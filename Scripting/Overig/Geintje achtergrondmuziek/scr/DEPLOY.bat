@echo off
echo Setting up installation folder
mkdir C:\src\

echo Copying script files
copy "%~dp0\script.ps1" "C:\src\script.ps1"
copy "%~dp0\sample.jpg" "C:\src\sample.jpg"
copy "%~dp0\sample.mp3" "C:\src\sample.mp3"

echo Adding scheduler task
REM WARNING: change the file path to match the powershell version on the destination system, or use powershell.exe w/o filepath
REM INFO: change domainname to the domain the script runs in, and username to the user the script should affect (must be local to the machine)
schtasks /create /tn "run script every 20 minutes" /tr "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -windowstyle hidden C:\src\script.ps1" /sc minute /mo 5 /RU domainname\username

echo Logging off
logoff.exe



