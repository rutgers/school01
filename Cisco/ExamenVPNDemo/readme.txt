Bouw de opstelling:
Verbind Fast- of Gigabitethernet0/0 van beide routers
Sluit 1 pc of 1 switch en een PC aan op de Fast- of Gigabitethernet0/1 van beide routers
Voer de configs van beide routers in en geef de PC's een IP adres

Testen:
Ping van PC naar de direct verbonden router
Dit moet werken voor het verder testen

Voer in: show crypto isakmp sa
Als de status van de tunnel: QM_IDLE  is dan is alles goed geconfigureerd

Voer in: show crypto ipsec sa
Dit zou er nu zo uit moeten zien:
#pkts encaps: 0, #pkts encrypt: 0, #pkts digest: 0
#pkts decaps: 0, #pkts decrypt: 0, #pkts verify: 0
Als de getallen niet 0 zijn copy dan de running config naar de startup config en reload de router


Ping nu een PC aan de andere kant van de tunnel
Voer in: show crypto ipsec sa
Dit zou er nu zo uit moeten zien: (getallen zijn mogelijk iets hoger of lager)
#pkts encaps: 8, #pkts encrypt: 8, #pkts digest: 0
#pkts decaps: 5, #pkts decrypt: 5, #pkts verify: 0
Als de getallen 0 zijn  na het pingen dan gaat het verkeer niet over de tunnel en is er iets fout gegaan

Bonus NAT:
Kijk eerst of het verkeer over de tunnel gaat
Voer in: show ip nat translations
Er zou nu niks moeten verschijnen (er is nog geen verkeer door NAT gegaan, alleen over de VPN tunnel)

Ga nu op een PC naar de command prompt en voer in: telnet [ip van de router aan de andere kant] en druk op enter
De verbinding wordt meteen verbroken (telnet is niet op die router geconfigureerd)
Voer in: show ip nat translations
Er zou nu een entry in de NAT tabel gemaakt moeten zijn voor het telnet verkeer

Bonus tip:
Download notepad++ en installeer de compare plugin, open nu 2 verschillende cisco configuraties en ga via het menu naar plugins->compare->compare
Je ziet nu de verschillen tussen de 2 configuraties


